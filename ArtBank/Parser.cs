﻿using ArtBank.Models;
using ArtBank.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ArtBank.Controllers
{
    public class Parser
    {

        public void GetFile()
        {
            ///Добавление данных из файла Daily
            var xdoc = XDocument.Load(@"Files\XML_daily.xml");
            foreach (var element in xdoc.Element("ValCurs").Elements("Valute"))
            {
                var id = element.Attribute("ID");
                var numCode = element.Element("NumCode");
                var charCode = element.Element("CharCode");
                var nominal = element.Element("Nominal");
                var name = element.Element("Name");
                var value = element.Element("Value");

                if (numCode != null && charCode != null && nominal != null && name != null && value != null)
                {
                    var _rate = new Rate
                    {
                        Id = id.Value,
                        NumCode = Convert.ToInt32(numCode.Value),
                        CharCode = charCode.Value,
                        Nominal = nominal.Value,
                        Name = name.Value,
                        Value = value.Value
                    };
                    using (var _db = new MainContext())
                    {
                        var rate = new Rate();
                        try
                        {
                            rate = _db.Rates.First(x => x.Id == _rate.Id);
                        }
                        catch(Exception e)
                        {
                            if (rate.Id == null)
                            {
                                _db.Rates.Add(_rate);
                                _db.SaveChanges();
                            }
                        }
                    }
                }
            }
        }
    }
}