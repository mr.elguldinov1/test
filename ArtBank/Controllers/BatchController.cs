﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using ArtBank.Models.Context;
using ArtBank.Models.contract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Polenter.Serialization;

namespace ArtBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BatchController : ControllerBase
    {
        MainContext _db;

        private readonly IHostingEnvironment _appEnvironment;
        public BatchController(MainContext db, IHostingEnvironment hostingEnvironment)
        {
            _db = db;
            _appEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public async Task<FileResult> Get(DateTime value)
        {
            var batchs = await _db.Batches
                .Include(x => x.Contract)
                .Include(x => x.Contract.ContractData)
                .Include(x => x.Contract.ContractData.TotalAmount)
                .Include(x => x.Contract.ContractData.TotalMonthlyPayment)
                .Include(x => x.Contract.ContractData.ProlongationAmount)
                .Include(x => x.Contract.SubjectRole)
                .Where(x => x.dateTime == value)
                .ToListAsync();
            var batch = new Batch();
            if (batchs.Count > 0)
            {
                batch = batchs.FirstOrDefault();
                int i = batchs.Count + 1;
                batch.BatchIdentifier = "Bank_" + value + "_" + i;
                batch.dateTime = value;
                var newBatch = new Batch
                {
                    ContractId = batch.ContractId,
                    dateTime = value,
                    BatchIdentifier = batch.BatchIdentifier
                };
                _db.Entry(newBatch).State = EntityState.Added;
                await _db.SaveChangesAsync();
                var dir = new DirectoryInfo("wwwroot");
                foreach (FileInfo file in dir.GetFiles())
                {
                    file.Delete();
                }
                var obj = (Object)batch;
                var serializer3 = new SharpSerializer();
                serializer3.Serialize(obj, @"wwwroot\test.xml");
            }
            else if (batchs.Count == 0)
            {
                batch = await _db.Batches.Include(x => x.Contract)
                .Include(x => x.Contract.ContractData)
                .Include(x => x.Contract.ContractData.TotalAmount)
                .Include(x => x.Contract.ContractData.TotalMonthlyPayment)
                .Include(x => x.Contract.ContractData.ProlongationAmount)
                .Include(x => x.Contract.SubjectRole)
                .FirstOrDefaultAsync();
                var newBatch = new Batch
                {
                    dateTime = value,
                    BatchIdentifier = "Bank_" + value + "_" + 1,
                    ContractId = batch.ContractId
                };
                _db.Entry(newBatch).State = EntityState.Added;
                await _db.SaveChangesAsync();
                var dir = new DirectoryInfo("wwwroot");
                foreach (FileInfo file in dir.GetFiles())
                {
                    file.Delete();
                }
                var obj = (Object)batch;
                var serializer3 = new SharpSerializer();
                serializer3.Serialize(obj, @"wwwroot\test.xml");
            }
            string path2 = Path.Combine(_appEnvironment.ContentRootPath, @"wwwroot\test.xml");
            var fs = new FileStream(path2, FileMode.Open);
            string file_type = "application/xml";
            string fileName = batch.BatchIdentifier + ".xml";
            return File(fs, file_type, fileName);
        }
    }
}