﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using ArtBank.Models.Context;
using ArtBank.Models.contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ArtBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractController : ControllerBase
    {
        MainContext _db;

        public ContractController(MainContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<IEnumerable<Contract>> Get()
        {
            return await _db.Contracts.ToListAsync();
        }

        [HttpPost]
        public void Post()
        {
            /// Добавление в БД из файла BANK_20180115_1.xml"
            /*string path = @"Files\BANK_20180115_1.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(Batch));
            StreamReader reader = new StreamReader(path);
            var batch = (Batch)serializer.Deserialize(reader);
            #region

            var contractData = new ContractData()
            {
                Branch = batch.Contract.ContractData.Branch,
                PhaseOfContract = batch.Contract.ContractData.PhaseOfContract,
                ContractStatus = batch.Contract.ContractData.ContractStatus,
                TypeOfContract = batch.Contract.ContractData.TypeOfContract,
                ContractSubtype = batch.Contract.ContractData.ContractSubtype,
                OwnershipType = batch.Contract.ContractData.OwnershipType,
                PurposeOfFinancing = batch.Contract.ContractData.PurposeOfFinancing,
                CurrencyOfContract = batch.Contract.ContractData.CurrencyOfContract,

                NextPaymentDate = batch.Contract.ContractData.NextPaymentDate,
                PaymentPeriodicity = batch.Contract.ContractData.PaymentPeriodicity,
                StartDate = batch.Contract.ContractData.StartDate,
                RestructuringDate = batch.Contract.ContractData.RestructuringDate,
                ExpectedEndDate = batch.Contract.ContractData.ExpectedEndDate,
                RealEndDate = batch.Contract.ContractData.RealEndDate,
                NegativeStatusOfContract = batch.Contract.ContractData.NegativeStatusOfContract,
                ProlongationDate = batch.Contract.ContractData.ProlongationDate
            };
            _db.ContractDatas.Add(contractData);
            _db.SaveChanges();

            var batch2 = new Batch
            {
                BatchIdentifier = batch.BatchIdentifier
            };
            _db.Batches.Add(batch2);
            _db.SaveChanges();

            var contract = new Contract()
            {
                ContractCode = batch.Contract.ContractCode,
                ContractDataId = contractData.Id,
                Id = 1
            };
            _db.Contracts.Add(contract);
            _db.SaveChanges();

            var totalAmount = new TotalAmount()
            {
                Value = batch.Contract.ContractData.TotalAmount.Value,
                Currency = batch.Contract.ContractData.TotalAmount.Currency,
                Id = 1,
            };
            _db.TotalAmounts.Add(totalAmount);
            _db.SaveChanges();

            var totalMonthly = new TotalMonthlyPayment()
            {
                Value = batch.Contract.ContractData.TotalMonthlyPayment.Value,
                Currency = batch.Contract.ContractData.TotalMonthlyPayment.Currency,
                Id = 1
            };
            _db.TotalMonthlyPayments.Add(totalMonthly);
            _db.SaveChanges();

            var prolongationAmount = new ProlongationAmount()
            {
                Value = batch.Contract.ContractData.ProlongationAmount.Value,
                Currency = batch.Contract.ContractData.ProlongationAmount.Currency,
                Id = 1
            };
            _db.ProlongationAmounts.Add(prolongationAmount);
            _db.SaveChanges();


            batch2.ContractId = contract.Id;
            _db.Entry(batch2).State = EntityState.Modified;
            _db.SaveChanges();
            contractData.TotalAmountId = totalAmount.Id;
            contractData.TotalMonthlyPaymentId = totalMonthly.Id;
            contractData.ProlongationAmountId = prolongationAmount.Id;
            _db.Entry(contractData).State = EntityState.Modified;
            _db.SaveChangesAsync();
            
            #endregion
            foreach (var item in batch.Contract.SubjectRole)
            {
                var subjectRole = new SubjectRole()
                {
                    ContractId = contract.Id,
                    CustomerCode = item.CustomerCode,
                    RoleOfCustomer = item.RoleOfCustomer,
                    RealEndDate = item.RealEndDate,
                    GuarantorRelationship = item.GuarantorRelationship
                };
                _db.SubjectRoles.Add(subjectRole);
                _db.SaveChanges();
            };*/
        }
    }
}