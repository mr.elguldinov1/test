﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArtBank.Models;
using ArtBank.Models.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ArtBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        MainContext _db;

        public ValuesController(MainContext db)
        {
            _db = db;
        }

        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<Rate>> Get()
        {
            return await _db.Rates.ToListAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<Rate> Get(string id)
        {
            return await _db.Rates.FirstAsync(x=>x.Id == id);
        }

        // POST api/values
        [HttpPost]
        public async Task Post()
        {
            Parser parser = new Parser();
            parser.GetFile();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
