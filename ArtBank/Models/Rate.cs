﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArtBank.Models
{
    public class Rate
    {
        /// <summary>
        /// Ид валюты
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// NumCode
        /// </summary>
        public int NumCode { get; set; }
        /// <summary>
        /// CharCode
        /// </summary>
        public string CharCode { get; set; }
        /// <summary>
        /// Номинал
        /// </summary>
        public string Nominal { get; set; }
        /// <summary>
        /// Наименование валюты
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }
    }
}
