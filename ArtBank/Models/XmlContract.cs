﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArtBank.Models
{
    public class XmlContract
    {

        // Примечание. Для запуска созданного кода может потребоваться NET Framework версии 4.5 или более поздней версии и .NET Core или Standard версии 2.0 или более поздней.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class Batch
        {

            private string batchIdentifierField;

            private BatchContract contractField;

            /// <remarks/>
            public string BatchIdentifier
            {
                get
                {
                    return this.batchIdentifierField;
                }
                set
                {
                    this.batchIdentifierField = value;
                }
            }

            /// <remarks/>
            public BatchContract Contract
            {
                get
                {
                    return this.contractField;
                }
                set
                {
                    this.contractField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class BatchContract
        {

            private string contractCodeField;

            private BatchContractContractData contractDataField;

            private BatchContractSubjectRole[] subjectRoleField;

            /// <remarks/>
            public string ContractCode
            {
                get
                {
                    return this.contractCodeField;
                }
                set
                {
                    this.contractCodeField = value;
                }
            }

            /// <remarks/>
            public BatchContractContractData ContractData
            {
                get
                {
                    return this.contractDataField;
                }
                set
                {
                    this.contractDataField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("SubjectRole")]
            public BatchContractSubjectRole[] SubjectRole
            {
                get
                {
                    return this.subjectRoleField;
                }
                set
                {
                    this.subjectRoleField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class BatchContractContractData
        {

            private string branchField;

            private string phaseOfContractField;

            private string contractStatusField;

            private string typeOfContractField;

            private string contractSubtypeField;

            private string ownershipTypeField;

            private string purposeOfFinancingField;

            private string currencyOfContractField;

            private BatchContractContractDataTotalAmount totalAmountField;

            private System.DateTime nextPaymentDateField;

            private BatchContractContractDataTotalMonthlyPayment totalMonthlyPaymentField;

            private object paymentPeriodicityField;

            private System.DateTime startDateField;

            private System.DateTime restructuringDateField;

            private System.DateTime expectedEndDateField;

            private object realEndDateField;

            private object negativeStatusOfContractField;

            private BatchContractContractDataProlongationAmount prolongationAmountField;

            private System.DateTime prolongationDateField;

            /// <remarks/>
            public string Branch
            {
                get
                {
                    return this.branchField;
                }
                set
                {
                    this.branchField = value;
                }
            }

            /// <remarks/>
            public string PhaseOfContract
            {
                get
                {
                    return this.phaseOfContractField;
                }
                set
                {
                    this.phaseOfContractField = value;
                }
            }

            /// <remarks/>
            public string ContractStatus
            {
                get
                {
                    return this.contractStatusField;
                }
                set
                {
                    this.contractStatusField = value;
                }
            }

            /// <remarks/>
            public string TypeOfContract
            {
                get
                {
                    return this.typeOfContractField;
                }
                set
                {
                    this.typeOfContractField = value;
                }
            }

            /// <remarks/>
            public string ContractSubtype
            {
                get
                {
                    return this.contractSubtypeField;
                }
                set
                {
                    this.contractSubtypeField = value;
                }
            }

            /// <remarks/>
            public string OwnershipType
            {
                get
                {
                    return this.ownershipTypeField;
                }
                set
                {
                    this.ownershipTypeField = value;
                }
            }

            /// <remarks/>
            public string PurposeOfFinancing
            {
                get
                {
                    return this.purposeOfFinancingField;
                }
                set
                {
                    this.purposeOfFinancingField = value;
                }
            }

            /// <remarks/>
            public string CurrencyOfContract
            {
                get
                {
                    return this.currencyOfContractField;
                }
                set
                {
                    this.currencyOfContractField = value;
                }
            }

            /// <remarks/>
            public BatchContractContractDataTotalAmount TotalAmount
            {
                get
                {
                    return this.totalAmountField;
                }
                set
                {
                    this.totalAmountField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime NextPaymentDate
            {
                get
                {
                    return this.nextPaymentDateField;
                }
                set
                {
                    this.nextPaymentDateField = value;
                }
            }

            /// <remarks/>
            public BatchContractContractDataTotalMonthlyPayment TotalMonthlyPayment
            {
                get
                {
                    return this.totalMonthlyPaymentField;
                }
                set
                {
                    this.totalMonthlyPaymentField = value;
                }
            }

            /// <remarks/>
            public object PaymentPeriodicity
            {
                get
                {
                    return this.paymentPeriodicityField;
                }
                set
                {
                    this.paymentPeriodicityField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime StartDate
            {
                get
                {
                    return this.startDateField;
                }
                set
                {
                    this.startDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime RestructuringDate
            {
                get
                {
                    return this.restructuringDateField;
                }
                set
                {
                    this.restructuringDateField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime ExpectedEndDate
            {
                get
                {
                    return this.expectedEndDateField;
                }
                set
                {
                    this.expectedEndDateField = value;
                }
            }

            /// <remarks/>
            public object RealEndDate
            {
                get
                {
                    return this.realEndDateField;
                }
                set
                {
                    this.realEndDateField = value;
                }
            }

            /// <remarks/>
            public object NegativeStatusOfContract
            {
                get
                {
                    return this.negativeStatusOfContractField;
                }
                set
                {
                    this.negativeStatusOfContractField = value;
                }
            }

            /// <remarks/>
            public BatchContractContractDataProlongationAmount ProlongationAmount
            {
                get
                {
                    return this.prolongationAmountField;
                }
                set
                {
                    this.prolongationAmountField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime ProlongationDate
            {
                get
                {
                    return this.prolongationDateField;
                }
                set
                {
                    this.prolongationDateField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class BatchContractContractDataTotalAmount
        {

            private ushort valueField;

            private string currencyField;

            /// <remarks/>
            public ushort Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            /// <remarks/>
            public string Currency
            {
                get
                {
                    return this.currencyField;
                }
                set
                {
                    this.currencyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class BatchContractContractDataTotalMonthlyPayment
        {

            private ushort valueField;

            private string currencyField;

            /// <remarks/>
            public ushort Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            /// <remarks/>
            public string Currency
            {
                get
                {
                    return this.currencyField;
                }
                set
                {
                    this.currencyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class BatchContractContractDataProlongationAmount
        {

            private byte valueField;

            private string currencyField;

            /// <remarks/>
            public byte Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }

            /// <remarks/>
            public string Currency
            {
                get
                {
                    return this.currencyField;
                }
                set
                {
                    this.currencyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class BatchContractSubjectRole
        {

            private string customerCodeField;

            private string roleOfCustomerField;

            private System.DateTime realEndDateField;

            private byte guarantorRelationshipField;

            /// <remarks/>
            public string CustomerCode
            {
                get
                {
                    return this.customerCodeField;
                }
                set
                {
                    this.customerCodeField = value;
                }
            }

            /// <remarks/>
            public string RoleOfCustomer
            {
                get
                {
                    return this.roleOfCustomerField;
                }
                set
                {
                    this.roleOfCustomerField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime RealEndDate
            {
                get
                {
                    return this.realEndDateField;
                }
                set
                {
                    this.realEndDateField = value;
                }
            }

            /// <remarks/>
            public byte GuarantorRelationship
            {
                get
                {
                    return this.guarantorRelationshipField;
                }
                set
                {
                    this.guarantorRelationshipField = value;
                }
            }
        }


    }


}
