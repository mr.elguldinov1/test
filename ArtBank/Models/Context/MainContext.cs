﻿using ArtBank.Models.contract;
using Microsoft.EntityFrameworkCore;

namespace ArtBank.Models.Context
{
    public class MainContext: DbContext
    {
        public DbSet<Rate> Rates { get; set; }
        public DbSet<TotalAmount> TotalAmounts { get; set; }
        public DbSet<TotalMonthlyPayment> TotalMonthlyPayments { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ContractData> ContractDatas { get; set; }
        public DbSet<ProlongationAmount> ProlongationAmounts { get; set; }

        //public DbSet<RootObject> RootObjects { get; set; }
        public DbSet<SubjectRole> SubjectRoles { get; set; }
        public DbSet<value> values { get; set; }

        public MainContext(DbContextOptions<MainContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(@"Data Source=OLAFLY-COMPANY;Initial Catalog=ArtBank;User ID=admin;Password=1994");
        }

        public MainContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContractData>()
                .HasOne(x => x.TotalAmount)
                .WithOne()
                .HasForeignKey<TotalAmount>(x => x.Id)
                .IsRequired(true);

            modelBuilder.Entity<ContractData>()
                .HasOne(x => x.TotalMonthlyPayment)
                .WithOne()
                .HasForeignKey<TotalMonthlyPayment>(x=>x.Id)
                .IsRequired(true);

            modelBuilder.Entity<ContractData>()
                .HasOne(x => x.ProlongationAmount)
                .WithOne()
                .HasForeignKey<ProlongationAmount>(x => x.Id)
                .IsRequired(true);

            modelBuilder.Entity<Contract>()
                .HasOne(x => x.ContractData)
                .WithMany(x=>x.Contracts)
                .HasForeignKey(s => s.ContractDataId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<SubjectRole>()
                .HasOne(x => x.Contract)
                .WithMany(x => x.SubjectRole)
                .HasForeignKey(x => x.ContractId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Batch>()
                .HasOne(x => x.Contract)
                .WithOne()
                .HasForeignKey<Contract>(x => x.Id)
                .IsRequired(true);
            /*

            modelBuilder.Entity<RootObject>()
                .HasOne(x => x.Batch)
                .WithOne()
                .HasForeignKey<Batch>(x => x.Id)
                .IsRequired(true);*/
        }
    }
}
