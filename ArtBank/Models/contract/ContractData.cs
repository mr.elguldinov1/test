﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ArtBank.Models.contract
{
    [Serializable]
    public class ContractData
    {
        public ContractData()
        {
            Contracts = new List<Contract>();
        }

        [Key]
        public int Id { get; set; }
        public string Branch { get; set; }
        public string PhaseOfContract { get; set; }
        public string ContractStatus { get; set; }
        public string TypeOfContract { get; set; }
        public string ContractSubtype { get; set; }
        public string OwnershipType { get; set; }
        public string PurposeOfFinancing { get; set; }
        public string CurrencyOfContract { get; set; }
        public int TotalAmountId { get; set; }
        public TotalAmount TotalAmount { get; set; }
        public string NextPaymentDate { get; set; }
        public int TotalMonthlyPaymentId { get; set; }
        public TotalMonthlyPayment TotalMonthlyPayment { get; set; }
        public string PaymentPeriodicity { get; set; }
        public string StartDate { get; set; }
        public string RestructuringDate { get; set; }
        public string ExpectedEndDate { get; set; }
        public string RealEndDate { get; set; }
        public string NegativeStatusOfContract { get; set; }
        public int ProlongationAmountId { get; set; }
        public ProlongationAmount ProlongationAmount { get; set; }
        public string ProlongationDate { get; set; }
        public virtual List<Contract> Contracts { get; set; }
    }
}
