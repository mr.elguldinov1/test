﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ArtBank.Models.contract
{
    public class RootObject
    {
        [Key]
        public int Id { get; set; }
        public int BatchId { get; set; }
        public Batch Batch { get; set; }
    }
}
