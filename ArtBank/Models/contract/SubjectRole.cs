﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ArtBank.Models.contract
{
    public class SubjectRole
    {
        [Key]
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string RoleOfCustomer { get; set; }
        public string RealEndDate { get; set; }
        public string GuarantorRelationship { get; set; }

        public int ContractId { get; set; }
        public Contract Contract { get; set; }
    }
}
