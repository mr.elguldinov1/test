﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ArtBank.Models.contract
{
    [Serializable]
    public class TotalAmount
    {
        /*public TotalAmount()
        {
            ContractDatas = new List<ContractData>();
        }*/

        [Key]
        public int Id { get; set; }
        public string Value { get; set; }
        public string Currency { get; set; }
        //public virtual List<ContractData> ContractDatas { get; set; }
    }
}
