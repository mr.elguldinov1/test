﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ArtBank.Models.contract
{
    public class ProlongationAmount
    {
        /*public ProlongationAmount()
        {
            ContractDatas = new List<ContractData>();
        }
        **/
        [Key]
        public int Id { get; set; }
        public string Value { get; set; }
        public string Currency { get; set; }
        //public virtual List<ContractData> ContractDatas { get; set; }
    }
}
