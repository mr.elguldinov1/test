﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ArtBank.Models.contract
{
    [Serializable]
    public class Contract
    {
        public Contract()
        {
            SubjectRole = new List<SubjectRole>();
        }
        [Key]
        public int Id { get; set; }
        public string ContractCode { get; set; }
        public int ContractDataId { get; set; }
        public ContractData ContractData { get; set; }
        [XmlElement("SubjectRole")]
        public virtual List<SubjectRole> SubjectRole { get; set; }
    }
}
