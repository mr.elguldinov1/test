﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ArtBank.Models.contract
{
    [Serializable]
    public class Batch
    {
        [Key]
        public int Id { get; set; }
        public DateTime dateTime { get; set; }
        public string BatchIdentifier { get; set; }
        public int ContractId { get; set; }
        public Contract Contract { get; set; }
    }
}
