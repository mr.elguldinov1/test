﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ArtBank.Migrations
{
    public partial class _1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Batches",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    dateTime = table.Column<DateTime>(nullable: false),
                    BatchIdentifier = table.Column<string>(nullable: true),
                    ContractId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Batches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractDatas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Branch = table.Column<string>(nullable: true),
                    PhaseOfContract = table.Column<string>(nullable: true),
                    ContractStatus = table.Column<string>(nullable: true),
                    TypeOfContract = table.Column<string>(nullable: true),
                    ContractSubtype = table.Column<string>(nullable: true),
                    OwnershipType = table.Column<string>(nullable: true),
                    PurposeOfFinancing = table.Column<string>(nullable: true),
                    CurrencyOfContract = table.Column<string>(nullable: true),
                    TotalAmountId = table.Column<int>(nullable: false),
                    NextPaymentDate = table.Column<string>(nullable: true),
                    TotalMonthlyPaymentId = table.Column<int>(nullable: false),
                    PaymentPeriodicity = table.Column<string>(nullable: true),
                    StartDate = table.Column<string>(nullable: true),
                    RestructuringDate = table.Column<string>(nullable: true),
                    ExpectedEndDate = table.Column<string>(nullable: true),
                    RealEndDate = table.Column<string>(nullable: true),
                    NegativeStatusOfContract = table.Column<string>(nullable: true),
                    ProlongationAmountId = table.Column<int>(nullable: false),
                    ProlongationDate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractDatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rates",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    NumCode = table.Column<int>(nullable: false),
                    CharCode = table.Column<string>(nullable: true),
                    Nominal = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "values",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_values", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contracts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    ContractCode = table.Column<string>(nullable: true),
                    ContractDataId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contracts_ContractDatas_ContractDataId",
                        column: x => x.ContractDataId,
                        principalTable: "ContractDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contracts_Batches_Id",
                        column: x => x.Id,
                        principalTable: "Batches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProlongationAmounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProlongationAmounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProlongationAmounts_ContractDatas_Id",
                        column: x => x.Id,
                        principalTable: "ContractDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TotalAmounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TotalAmounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TotalAmounts_ContractDatas_Id",
                        column: x => x.Id,
                        principalTable: "ContractDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TotalMonthlyPayments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TotalMonthlyPayments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TotalMonthlyPayments_ContractDatas_Id",
                        column: x => x.Id,
                        principalTable: "ContractDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerCode = table.Column<string>(nullable: true),
                    RoleOfCustomer = table.Column<string>(nullable: true),
                    RealEndDate = table.Column<string>(nullable: true),
                    GuarantorRelationship = table.Column<string>(nullable: true),
                    ContractId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectRoles_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_ContractDataId",
                table: "Contracts",
                column: "ContractDataId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectRoles_ContractId",
                table: "SubjectRoles",
                column: "ContractId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProlongationAmounts");

            migrationBuilder.DropTable(
                name: "Rates");

            migrationBuilder.DropTable(
                name: "SubjectRoles");

            migrationBuilder.DropTable(
                name: "TotalAmounts");

            migrationBuilder.DropTable(
                name: "TotalMonthlyPayments");

            migrationBuilder.DropTable(
                name: "values");

            migrationBuilder.DropTable(
                name: "Contracts");

            migrationBuilder.DropTable(
                name: "ContractDatas");

            migrationBuilder.DropTable(
                name: "Batches");
        }
    }
}
