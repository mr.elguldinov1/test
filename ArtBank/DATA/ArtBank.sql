USE [master]
GO

/****** Object:  Database [ArtBank]    Script Date: 04.07.2019 13:40:08 ******/
CREATE DATABASE [ArtBank]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ArtBank', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ArtBank.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ArtBank_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\ArtBank_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [ArtBank] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ArtBank].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ArtBank] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [ArtBank] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [ArtBank] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [ArtBank] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [ArtBank] SET ARITHABORT OFF 
GO

ALTER DATABASE [ArtBank] SET AUTO_CLOSE ON 
GO

ALTER DATABASE [ArtBank] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [ArtBank] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [ArtBank] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [ArtBank] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [ArtBank] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [ArtBank] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [ArtBank] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [ArtBank] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [ArtBank] SET  ENABLE_BROKER 
GO

ALTER DATABASE [ArtBank] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [ArtBank] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [ArtBank] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [ArtBank] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [ArtBank] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [ArtBank] SET READ_COMMITTED_SNAPSHOT ON 
GO

ALTER DATABASE [ArtBank] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [ArtBank] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [ArtBank] SET  MULTI_USER 
GO

ALTER DATABASE [ArtBank] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [ArtBank] SET DB_CHAINING OFF 
GO

ALTER DATABASE [ArtBank] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [ArtBank] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [ArtBank] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [ArtBank] SET QUERY_STORE = OFF
GO

ALTER DATABASE [ArtBank] SET  READ_WRITE 
GO

